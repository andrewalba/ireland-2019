# Ireland Wish List

List of top places I would like to tour or hike while in Ireland. Depending on the flight options, the order of locations to visit can change.

We are still looking for a nice 3 day/ 4 night walking tour if interested.

## Dublin

Plan would be to spend a good day in Dublin walking about as much as possible. Haven't looked at specifics in Dublin, but woudl be interested in history so any muesums or galleries would be fun. Also need to find some good restaurants and places to snack on Irish food.

### Jameson Distillery Bow St.

The original Jameson distillery in Dublin is now just a visitors center that offers guided tours and whiskey tastings, but no actual distilling on premise.

## Rock of Cashel

South and west of Dublin you will find Rock of Cashel. Legend is the Rock of Cashel originated in the Devil's Bit, a mountain 20 miles (30 km) north of Cashel when St. Patrick banished Satan from a cave, resulting in the Rock's landing in Cashel.[1] Cashel is reputed to be the site of the conversion of the King of Munster by St. Patrick in the 5th century.

Some of the buildings date back to 1100 and the area has an extensive graveyard with many high crosses.

## Jameson Distillery _Country Cork_

Located about 30 minutes of Cork is one of two Jameson Distilleries in Ireland. I chose to add this second Jameson distillery as this is the new distillery home.

## Charles Fort

Heading south to the southern coast is the Desmond Castle. The castle was built in 1500's and was used for various purposes until late 1800's. After falling into decay the castle was declared a national monument in 1938 and now is host to the International Museum of Wine Exhibition which shines a light on Irelands unique wine links to Europe and beyond.

South of the castle lies the 300 year old star-shaped military fortress that stands guard over the Kinsale harbor. 

Across the harbor is the even older James Fort Kinsale which was built in 1607.

If this was a place to stop, here are some [AirBnB](https://www.airbnb.com/s/Kinsale/homes?refinement_paths%5B%5D=%2Ffor_you&allow_override%5B%5D=&room_types%5B%5D=Entire%20home%2Fapt&ne_lat=51.72267809899598&ne_lng=-8.497695075818683&sw_lat=51.697201397654815&sw_lng=-8.536318885633136&zoom=14&search_by_map=true&s_tag=NiLvLD39)

## Killarney National Park | Kenmare | Dingle

Heading to the west towards Killarney National Park, ifi time alots, I would like to stop in Kenmare and see the Kenmare Stone Circle. This isn't necessary as I only want to stop there because I lived in Kenmare, ND.

A nice bonus would be the Torc Waterfall, which is only five miles off the path between Kenmare to Killarney. If continue a walk up and past the falls, you are rewarded with a nice view of the surrounding lakes.

Killarney might be a good base for a few days. There are many guided tours in the area and hopefully we could find some "un-guided" tours as well. 

_What happened to [Ancient Ways Tours](http://www.ancientways.ie/)?_

Would like to find a portion of the Dingle way to hike for a day. Would have to reseach this for some time as normal tour is 8 days and 111 miles. The trail is one of the best in Ireland so hopefully find one or two **sweet spots** won't be to hard. 

We should be sure to take a drive while up in Dingle pennisula on the Slea Head Drive which is supposedly one of the most beautfiul roads in Ireland.

[Dingle Distillery](http://www.dingledistillery.ie/whiskey/) which has single malt Irish Whiskey.

(Killarney AirBnB)[https://www.airbnb.com/s/Killarney--Ireland/homes?place_id=ChIJ2W6XtAw8RUgRwHQxl6nHAAo&refinement_paths%5B%5D=%2Ffor_you&allow_override%5B%5D=&room_types%5B%5D=Entire%20home%2Fapt&s_tag=MRAHHDGs]

## Dingle to Antrim Coast

Lots of options here and personally I have not preference.

One option would be to head inland to the East and jump on a trail of the Sliee Bloom mountains. Another option is to choose one of the Lough Boora Park trails. Both of these options are near Tullamore which means... you guessed it... we would be in close proximaty Tullamore Dew Distillery.

## Antrim Coast and Old Bushmills

Not real particular about how we get here, but just want to make sure this is on the list of places to visit.

### Carrick-a-Rede Rope Bridge / Giant's Causeway to Dunluce Castle

The Antrim coast has some great trails, but a highlight seems to be the Giant's Causeway. This is just a small section of the 32 mile **Causeway Coast Way*# trail. 

If we could find a bus or other transportation from Bushmills to Carrick-a-Rede rope bridge the most exciting option would be to start at Carrick-a-Reded rope brdiget and walk the coast to Giants Causway and then to Dunluce Caslte which is _about 12 miles_. From there it is a 2.5 mile walk back to Bushmills.

If that is too long of a walk for everyone, then option two would be to split the walk up. We would drive up to Carrick-a-Rede rope bridge and spend some time walking the coastline there and then drive back to Bushmills. From there we could get public transportation or taxi to bring us to the Giant's Causeway where we could then walk the coastline down to Dunluce Castle _about 5.1 miles_. From Dunluce Castle it is only a 2.5 mile walk back to Bushmills.

Bushmills has several five star AirBnB rentals around $100 a night with 3-4 beds [AirBnB](https://www.airbnb.com/s/Bushmills--United-Kingdom/homes?place_id=ChIJrYSARy0pYEgRu6YTu3L3ezQ&refinement_paths%5B%5D=%2Ffor_you&allow_override%5B%5D=&s_tag=geBmdkW2). Two that I thought looked interesting were the [Riverview](https://www.airbnb.com/rooms/4176622?location=Bushmills%2C%20United%20Kingdom) 4 bedroom home which has king size bed and beautfiul location and the [Distillery House](https://www.airbnb.com/rooms/5952114?location=Bushmills%2C%20United%20Kingdom&s=geBmdkW2) which is right next to Bushmills Distillery and allows Jason and I to enjoy many a Bushmill at the distillery [https://www.bushmills.com/distillery/].

Here are five other walks that woudl be interesting to look at [Walks in Ireland: six to savour](http://www.ireland.com/what-is-available/walking-and-hiking/articles/six-walks/)

## Belfast

Belfast is a large city with a historic past. Here is a short list of places we can visit while in Belfast.

* Titanic Belfast - Where the iconic ship was built.
* St George's Market - best on Firday, Satureday and Sunday. Incredible food selection.
* [Crumlin Road Goal](http://www.crumlinroadgaol.com/)
* Ulster Scots Center.
* Cave Hill Country Park - Features Belfast Castle, The White Stone and McArt's Fort and a great view if you make it to the top.
* Ulster Mueusum.
* Botanical Gardens which located close to Ulster Muesum.
